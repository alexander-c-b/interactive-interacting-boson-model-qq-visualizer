from pathlib import Path
from tkinter import Tk
from tkinter.filedialog import askopenfilename
from typing import Any, TypedDict, cast

import numpy as np
import pandas
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from matplotlib.widgets import Slider

from visualize_ibm_2_hamiltonian.generate_data import Array, get_epsilon_2_and_gamma
from visualize_ibm_2_hamiltonian.visualize import display_on_axes


def get_tac_path() -> Path:
    root = Tk()
    root.withdraw()
    return Path(
        askopenfilename(
            title="Select TAC file",
            filetypes=[("TAC files", pattern) for pattern in [".tsv", ".csv", ".txt"]],
        )
    )


def read_tac(path: Path) -> Array:
    if path.suffix in [".tsv", ".txt"]:
        delimiter = r"\s+"
    elif path.suffix == ".csv":
        delimiter = ","
    return (
        pandas.read_csv(path, header=None, skiprows=2, delimiter=delimiter)
        .to_numpy()[:, -1]
        .reshape((26, 21))
    )


class DisplayAxes(TypedDict):
    cutoff_slider: Axes
    axis_3d: Any
    axis_colormesh: Axes
    axis_wedge_gray: Axes
    axis_raw_image: Axes


def get_tac_axes() -> tuple[Figure, DisplayAxes]:
    fig, axd = plt.subplot_mosaic(
        [
            ["cutoff_slider", "cutoff_slider"],
            ["axis_3d", "axis_colormesh"],
            ["axis_wedge_gray", "axis_raw_image"],
        ],
        per_subplot_kw={
            "axis_3d": {
                "projection": "3d",
            },
            "axis_colormesh": {
                "projection": "polar",
            },
            "axis_wedge_gray": {
                "projection": "polar",
            },
        },
        figsize=(7, 7),
        height_ratios=[1, 4, 4],
        layout="constrained",
    )
    return fig, cast(DisplayAxes, axd)


def display_tac() -> None:
    path = get_tac_path()
    if not path:
        return
    tac = read_tac(path)
    tac = tac - tac.min()
    epsilon_2, gamma = get_epsilon_2_and_gamma(
        epsilon_points=tac.shape[0], gamma_points=tac.shape[1]
    )
    fig, axes = get_tac_axes()
    cutoff_slider = Slider(
        ax=axes["cutoff_slider"],
        label="Cutoff (MeV)",
        valmin=0,
        valmax=30,
        valinit=1,
        valstep=0.1,
        dragging=True,
    )

    def update(*args: Any) -> None:
        clipped: Array = tac.clip(None, cutoff_slider.val)
        potential_masked = np.ma.masked_where(clipped >= cutoff_slider.val, clipped)
        axes["axis_3d"].clear()
        axes["axis_colormesh"].clear()
        display_on_axes(
            epsilon_2,
            gamma,
            clipped,
            potential_3d=potential_masked,
            potential_contour=potential_masked,
            axis_3d=axes["axis_3d"],
            axis_colormesh=axes["axis_colormesh"],
            axis_wedge_gray=axes["axis_wedge_gray"],
            axis_raw_image=axes["axis_raw_image"],
        )

    cutoff_slider.on_changed(update)
    update()
    fig.suptitle(path.stem, fontsize="xx-large")
    plt.show(block=True)


if __name__ == "__main__":
    display_tac()
