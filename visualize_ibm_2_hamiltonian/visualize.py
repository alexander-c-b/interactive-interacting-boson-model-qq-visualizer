import re
from itertools import chain
from typing import Any, Final, cast

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from matplotlib.widgets import CheckButtons, RadioButtons, Slider, Widget

from visualize_ibm_2_hamiltonian.generate_data import (
    Array,
    get_epsilon_2_and_gamma,
    hamiltonian_functions,
    hamiltonian_labels,
)
from visualize_ibm_2_hamiltonian.plot_2d import plot_pie_wedge

allow_dragging: Final = True


def interactive_figure(
    *, allow_dragging: bool = True
) -> tuple[Figure, tuple[Widget, ...]]:
    fig, axd = plt.subplot_mosaic(
        [
            ["zeta", "zeta", "radio"],
            ["chi", "chi", "radio"],
            ["k_3", "k_3", "radio"],
            ["n", "n", "radio"],
            ["c_beta", "c_beta", "coefficients"],
            ["c_e", "c_e", "coefficients"],
            ["cutoff", "cutoff", "coefficients"],
            ["wedge_3d", "wedge_2d", "coefficients"],
            ["wedge_2d_gray", "raw_image", "coefficients"],
        ],
        per_subplot_kw={
            "wedge_3d": {"projection": "3d"},
            "wedge_2d": {"projection": "polar"},
            "wedge_2d_gray": {"projection": "polar"},
        },
        layout="constrained",
        figsize=(10, 10),
        width_ratios=[1, 1, 1],
        height_ratios=[
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            14,
            14,
        ],
    )
    axd = cast(dict[str, Axes], axd)
    axis_3d = cast(Any, axd["wedge_3d"])
    axis_colormesh = axd["wedge_2d"]
    axis_wedge_gray = axd["wedge_2d_gray"]
    axis_raw_image = axd["raw_image"]
    zeta_slider = Slider(
        ax=axd["zeta"],
        label=r"$\zeta$",
        valmin=0,
        valmax=1,
        valinit=1,
        valstep=0.025,
        dragging=allow_dragging,
    )
    chi_slider = Slider(
        ax=axd["chi"],
        label=r"$\chi$",
        valmin=-np.sqrt(7) / 2,
        valmax=np.sqrt(7) / 2,
        valinit=0,
        valstep=0.025,
        dragging=allow_dragging,
    )
    k_3_slider = Slider(
        ax=axd["k_3"],
        label=r"$k_3$",
        valmin=-20,
        valmax=20,
        valinit=0,
        valstep=0.025,
        dragging=allow_dragging,
    )
    n_slider = Slider(
        ax=axd["n"],
        label="$N$",
        valmin=2,
        valmax=50,
        valinit=10,
        valstep=2.0,
        dragging=allow_dragging,
    )
    c_beta_slider = Slider(
        ax=axd["c_beta"],
        label=r"$c_\beta$",
        valmin=0,
        valmax=10,
        valinit=1,
        valstep=0.25,
        dragging=allow_dragging,
    )
    c_e_slider = Slider(
        ax=axd["c_e"],
        label="$c_E$ (MeV)",
        valmin=0,
        valmax=30,
        valinit=1,
        valstep=0.25,
        dragging=allow_dragging,
    )
    cutoff_slider = Slider(
        ax=axd["cutoff"],
        label="Cutoff (MeV)",
        valmin=0,
        valmax=30,
        valinit=1,
        valstep=0.1,
        dragging=allow_dragging,
    )
    coefficient_text = axd["coefficients"].text(
        x=0.5,
        y=0.5,
        s="",
        verticalalignment="center",
        horizontalalignment="right",
        fontsize="x-large",
        linespacing=2,
    )
    axd["radio"].axis("off")
    axd["coefficients"].axis("off")
    check_axis = axd["radio"].inset_axes(bounds=(0.45, 0.85, 0.45, 0.1))
    resolution_buttons = CheckButtons(check_axis, ["Low-Res"])
    check_axis.axis("off")
    radio_buttons = RadioButtons(axd["radio"], tuple(hamiltonian_labels.values()))
    for label in chain(resolution_buttons.labels, radio_buttons.labels):
        label.set_fontsize("x-large")

    def update(label: Any = None) -> None:
        axis_3d.clear()
        axis_colormesh.clear()
        resolution = {True: 31, False: 101}[resolution_buttons.get_status()[0]]
        epsilon_2, gamma = get_epsilon_2_and_gamma(
            epsilon_points=resolution, gamma_points=resolution
        )
        potential_function = hamiltonian_functions[
            hamiltonian_labels.inverse[radio_buttons.value_selected]
        ]
        coefficients = potential_function(
            zeta=zeta_slider.val,
            chi=chi_slider.val,
            k_3=k_3_slider.val,
            c_e=c_e_slider.val,
            n=n_slider.val,
            result_type="named_coefficients",
        )
        initial_potential = potential_function(
            epsilon_2=epsilon_2,
            gamma=gamma,
            zeta=zeta_slider.val,
            chi=chi_slider.val,
            k_3=k_3_slider.val,
            c_beta=c_beta_slider.val,
            c_e=c_e_slider.val,
            n=n_slider.val,
        )
        potential: Array = (initial_potential - initial_potential.min()).clip(
            None, cutoff_slider.val
        )
        potential_masked = np.ma.masked_where(potential >= cutoff_slider.val, potential)
        display_on_axes(
            epsilon_2,
            gamma,
            potential,
            potential_3d=potential_masked,
            potential_contour=potential_masked,
            axis_3d=axis_3d,
            axis_colormesh=axis_colormesh,
            axis_wedge_gray=axis_wedge_gray,
            axis_raw_image=axis_raw_image,
        )
        coefficient_text.set_text(
            "\n".join(
                rf"${re.sub(r'(\d+)', r'{\1}', key)} = {value:+.3f}$"
                for key, value in coefficients.items()
            )
        )
        plt.draw()

    update()

    sliders = (
        zeta_slider,
        chi_slider,
        k_3_slider,
        n_slider,
        c_e_slider,
        c_beta_slider,
        cutoff_slider,
    )
    for slider in sliders:
        slider.on_changed(update)
    radio_buttons.on_clicked(update)
    resolution_buttons.on_clicked(update)

    return fig, (*sliders, radio_buttons, resolution_buttons)


def display_on_axes(
    epsilon_2: Array,
    gamma: Array,
    potential: Array,
    *,
    potential_3d: Array | None = None,
    potential_contour: Array | None = None,
    axis_3d: Any | None = None,
    axis_colormesh: Axes | None = None,
    axis_wedge_gray: Axes | None = None,
    axis_raw_image: Axes | None = None,
):
    x = epsilon_2 * np.cos(gamma)
    y = epsilon_2 * np.sin(gamma)
    potential_3d = potential_3d if potential_3d is not None else potential
    potential_contour = (
        potential_contour if potential_contour is not None else potential
    )
    if axis_3d is not None:
        axis_3d.plot_surface(x, y, potential_3d, cmap="rainbow")
    if axis_colormesh is not None:
        plot_pie_wedge(
            axis=axis_colormesh,
            gamma=gamma,
            epsilon_2=epsilon_2,
            function=potential_contour,
            plot_type="contour",
        )
    if axis_wedge_gray is not None:
        plot_pie_wedge(
            axis=axis_wedge_gray,
            gamma=gamma,
            epsilon_2=epsilon_2,
            function=potential,
            plot_type="colormesh",
            color_map="gray",
        )
    if axis_raw_image is not None:
        axis_raw_image.imshow(
            potential.transpose(),
            cmap="gray",
            origin="lower",
        )
