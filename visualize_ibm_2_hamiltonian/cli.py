import matplotlib
import matplotlib.backends.backend_tkagg
import matplotlib.pyplot as plt

from visualize_ibm_2_hamiltonian.visualize import interactive_figure


def cli() -> None:
    matplotlib.use("TkAgg")
    # Use a throw-away variable to prevent garbage collection of the figure (and more
    # importantly, its sliders)
    _ = interactive_figure()
    plt.show(block=True)
