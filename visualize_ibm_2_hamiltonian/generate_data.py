import enum
from functools import partial
from string import Template
from textwrap import dedent
from typing import (
    Callable,
    Final,
    Literal,
    Protocol,
    TypeAlias,
    TypeVar,
    cast,
    overload,
)

import numpy as np
from bidict import frozenbidict
from frozendict import frozendict
from numpy.typing import NDArray

Numeric: TypeAlias = float | NDArray[np.float64]
Array: TypeAlias = NDArray[np.float64]


def get_epsilon_2_and_gamma(
    *, gamma_points: int = 301, epsilon_points: int = 301
) -> tuple[Array, Array]:
    epsilon_2, gamma = np.mgrid[
        0 : 0.5 : epsilon_points * 1j, 0 : np.pi / 3 : gamma_points * 1j
    ].astype(np.float32)
    return (epsilon_2, gamma)


class HamiltonianFunction(Protocol):
    @overload
    def __call__(
        self,
        epsilon_2: Array,
        gamma: Array,
        *,
        zeta: Array | float,
        chi: Array | float,
        k_3: Array | float,
        c_beta: Array | float,
        c_e: Array | float,
        n: Array | float,
        result_type: Literal["number"] = ...,
    ) -> Array: ...

    @overload
    def __call__(
        self,
        epsilon_2: float,
        gamma: float,
        *,
        zeta: float,
        chi: float,
        k_3: float,
        c_beta: float,
        c_e: float,
        n: float,
        result_type: Literal["number"] = ...,
    ) -> float: ...

    @overload
    def __call__(
        self,
        *,
        zeta: float,
        chi: float,
        k_3: float,
        c_e: float,
        n: float,
        result_type: Literal["named_coefficients"] = ...,
    ) -> dict[str, float]: ...

    def __call__(
        self,
        epsilon_2: Array | float | None = None,
        gamma: Array | float | None = None,
        *,
        zeta: Array | float,
        chi: Array | float,
        k_3: Array | float,
        c_beta: Array | float | None = None,
        c_e: Array | float,
        n: Array | float,
        result_type: Literal["named_coefficients"] | Literal["number"] = "number",
    ) -> Array | float | dict[str, float]: ...


K = TypeVar("K")
V = TypeVar("V")
V2 = TypeVar("V2")


def map_values(transform: Callable[[V], V2], items: dict[K, V]) -> dict[K, V2]:
    return {key: transform(value) for key, value in items.items()}


@overload
def e_ibm_qq(
    epsilon_2: Array,
    gamma: Array,
    *,
    zeta: Array | float,
    chi: Array | float,
    k_3: Array | float,
    c_beta: Array | float,
    c_e: Array | float,
    n: Array | float,
    result_type: Literal["number"] = ...,
) -> Array: ...


@overload
def e_ibm_qq(
    epsilon_2: float,
    gamma: float,
    *,
    zeta: float,
    chi: float,
    k_3: float,
    c_beta: float,
    c_e: float,
    n: float,
    result_type: Literal["number"] = ...,
) -> float: ...


@overload
def e_ibm_qq(
    *,
    zeta: float,
    chi: float,
    k_3: float,
    c_e: float,
    n: float,
    result_type: Literal["named_coefficients"] = ...,
) -> dict[str, float]: ...


def e_ibm_qq(
    epsilon_2: Array | float | None = None,
    gamma: Array | float | None = None,
    *,
    zeta: Array | float,
    chi: Array | float,
    k_3: Array | float,
    c_e: Array | float,
    n: Array | float,
    c_beta: Array | float | None = None,
    result_type: Literal["named_coefficients", "number"] = "number",
) -> Array | float | dict[str, float]:
    s_1 = c_e * (-5 / 4) * zeta
    s_2 = c_e * ((1 - zeta) * n - 1 / 4 * zeta * (1 + chi**2))
    d_1 = -c_e * zeta * (n - 1)
    d_2 = c_e * zeta * (n - 1) * np.sqrt(2 / 7) * chi
    d_3 = -c_e * (zeta * (n - 1)) * (chi**2 / 14)
    if result_type == "named_coefficients":
        return map_values(
            float,
            {
                "s_1": s_1,
                "s_2": s_2,
                "d_1": d_1,
                "d_2": d_2,
                "d_3": d_3,
            },
        )
    elif result_type == "number":
        assert epsilon_2 is not None
        assert c_beta is not None
        assert gamma is not None
        beta = c_beta * epsilon_2
        return (
            s_1 / (1 + beta**2)
            + s_2 * beta**2 / (1 + beta**2)
            + d_1 * beta**2 / (1 + beta**2) ** 2
            + d_2 * beta**3 * np.cos(3 * gamma) / (1 + beta**2) ** 2
            + d_3 * beta**4 / (1 + beta**2) ** 2
        )


@overload
def e_ibm_qqq(
    epsilon_2: Array,
    gamma: Array,
    *,
    zeta: Array | float,
    chi: Array | float,
    k_3: Array | float,
    c_beta: Array | float,
    c_e: Array | float,
    n: Array | float,
    result_type: Literal["number"] = ...,
) -> Array: ...


@overload
def e_ibm_qqq(
    epsilon_2: float,
    gamma: float,
    *,
    zeta: float,
    chi: float,
    k_3: float,
    c_beta: float,
    c_e: float,
    n: float,
    discard_t1_t2: bool = False,
    result_type: Literal["number"] = ...,
) -> float: ...


@overload
def e_ibm_qqq(
    *,
    zeta: float,
    chi: float,
    k_3: float,
    c_e: float,
    n: float,
    result_type: Literal["named_coefficients"] = ...,
) -> dict[str, float]: ...


def e_ibm_qqq(
    epsilon_2: Array | float | None = None,
    gamma: Array | float | None = None,
    *,
    zeta: Array | float,
    chi: Array | float,
    k_3: Array | float,
    c_e: Array | float,
    n: Array | float,
    c_beta: Array | float | None = None,
    discard_t1_t2: bool = False,
    result_type: Literal["named_coefficients", "number"] = "number",
) -> Array | float | dict[str, float]:
    s_1 = c_e * (-5 / 4) * zeta
    s_2 = c_e * ((1 - zeta) * n - 1 / 4 * zeta * (1 + chi**2))
    d_1 = -c_e * zeta * (n - 1)
    d_2 = c_e * zeta * (n - 1) * np.sqrt(2 / 7) * chi
    d_3 = -c_e * (zeta * (n - 1)) * (chi**2 / 14)
    t_1_to_5_base = c_e * zeta * k_3 / (4 * n**2)
    t_1 = -t_1_to_5_base * (n / np.sqrt(5) * chi * 5)
    t_2 = -t_1_to_5_base * (n / (14 * np.sqrt(5)) * (28 * chi - 3 * chi**3))
    t_3 = -t_1_to_5_base * (n * (n - 1) * 3 * 14**2 * chi / (49 * np.sqrt(5)))
    t_4 = (
        t_1_to_5_base
        * (n * (n - 1) / (49 * np.sqrt(5)))
        * 3
        * np.sqrt(14)
        * (14 + 11 * chi**2)
    )
    t_5 = -t_1_to_5_base * n * (n - 1) / (49 * np.sqrt(5)) * 3 * (14 * chi - 3 * chi**3)
    t_6_to_10_base = c_e * zeta * k_3 / n**2 * n * (n - 1) * (n - 2) / (49 * np.sqrt(5))
    t_6 = -t_6_to_10_base * 42 * chi
    t_7 = t_6_to_10_base * np.sqrt(14) * 14
    t_8 = t_6_to_10_base * np.sqrt(14) * 3 * chi**2
    t_9 = -t_6_to_10_base * 2 * chi**3
    t_10 = t_6_to_10_base * chi**3
    if result_type == "named_coefficients":
        return map_values(
            float,
            {
                "s_1": s_1,
                "s_2": s_2,
                "d_1": d_1,
                "d_2": d_2,
                "d_3": d_3,
            }
            | (
                {
                    "t_1": t_1,
                    "t_2": t_2,
                    "t_3": t_3,
                    "t_4": t_4,
                    "t_5": t_5,
                }
                if not discard_t1_t2
                else {}
            )
            | {
                "t_6": t_6,
                "t_7": t_7,
                "t_8": t_8,
                "t_9": t_9,
                "t_10": t_10,
            },
        )
    elif result_type == "number":
        assert epsilon_2 is not None
        assert c_beta is not None
        assert gamma is not None
        beta = c_beta * epsilon_2
        if discard_t1_t2:
            t_1 = 0
            t_2 = 0
            t_3 = 0
            t_4 = 0
            t_5 = 0
        return (
            (s_1 + t_1) / (1 + beta**2)
            + (s_2 + t_2) * beta**2 / (1 + beta**2)
            + (d_1 + t_3) * beta**2 / (1 + beta**2) ** 2
            + (d_2 + t_4) * beta**3 * np.cos(3 * gamma) / (1 + beta**2) ** 2
            + (d_3 + t_5) * beta**4 / (1 + beta**2) ** 2
            + t_6 * beta**4 / (1 + beta**2) ** 3
            + t_7 * beta**3 * np.cos(3 * gamma) / (1 + beta**2) ** 3
            + t_8 * beta**5 * np.cos(3 * gamma) / (1 + beta**2) ** 3
            + t_9 * beta**6 * np.cos(3 * gamma) ** 2 / (1 + beta**2) ** 3
            + t_10 * beta**6 / (1 + beta**2) ** 3
        )


class Hamiltonian(enum.Enum):
    QQ = enum.auto()
    QQQ_PLUS_T1_T2_T3 = enum.auto()
    QQQ_PLUS_T3 = enum.auto()


hamiltonian_labels: Final = frozenbidict(
    {
        Hamiltonian.QQ: "QQ",
        Hamiltonian.QQQ_PLUS_T3: r"$QQ + t_3$",
        Hamiltonian.QQQ_PLUS_T1_T2_T3: r"$QQ + \sum_{i=1}^3 {t_i}$",
    }
)

hamiltonian_functions: Final[frozendict[Hamiltonian, HamiltonianFunction]] = frozendict(
    {
        Hamiltonian.QQ: e_ibm_qq,
        Hamiltonian.QQQ_PLUS_T3: cast(
            HamiltonianFunction, partial(e_ibm_qqq, discard_t1_t2=True)
        ),
        Hamiltonian.QQQ_PLUS_T1_T2_T3: e_ibm_qqq,
    }
)


def display_qqq_coefficients(
    *, zeta: float, chi: float, n: float, k_3: float, c_e: float
) -> None:
    template = Template(
        dedent(
            f"""\
                term 1   $s_1   $t_1
                term 2   $s_2   $t_2
                term 3   $d_1   $t_3
                term 4   $d_2   $t_4
                term 5   $d_3   $t_5
                term 6   {' '*13}$t_6
                term 7   {' '*13}$t_7
                term 8   {' '*13}$t_8
                term 9   {' '*13}$t_9
                term 10   {' '*12}$t_10
            """.rstrip()
        )
    )
    print(
        template.substitute(
            map_values(
                lambda value: f"{value: >10.5f}",
                e_ibm_qqq(
                    zeta=zeta,
                    chi=chi,
                    k_3=k_3,
                    n=n,
                    c_e=c_e,
                    result_type="named_coefficients",
                ),
            )
        )
    )
