from typing import Literal, overload

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.figure import Figure

from visualize_ibm_2_hamiltonian.generate_data import Array


@overload
def plot_pie_wedge(
    gamma: Array,
    epsilon_2: Array,
    function: Array,
    *,
    axis: None = None,
    grid_lines: bool = True,
    axis_labels: bool = True,
    add_point_locations_scatter: bool = False,
    add_minima_scatter: bool = False,
    color_map: str = "gray",
    plot_type: Literal["contour", "colormesh"] = "contour",
    **kwargs,
) -> Figure: ...


@overload
def plot_pie_wedge(
    gamma: Array,
    epsilon_2: Array,
    function: Array,
    *,
    axis: plt.Axes,
    grid_lines: bool = True,
    axis_labels: bool = True,
    add_point_locations_scatter: bool = False,
    add_minima_scatter: bool = False,
    color_map: str = "gray",
    plot_type: Literal["contour", "colormesh"] = "contour",
    **kwargs,
) -> plt.Axes: ...


def plot_pie_wedge(
    gamma: Array,
    epsilon_2: Array,
    function: Array,
    *,
    axis: plt.Axes | None = None,
    plot_type: Literal["contour", "colormesh"] = "contour",
    grid_lines: bool = True,
    axis_labels: bool = True,
    add_point_locations_scatter: bool = False,
    add_minima_scatter: bool = False,
    color_map: str = "gray",
    **kwargs,
) -> Figure | plt.Axes:
    if axis is None:
        result, axis = plt.subplots(subplot_kw={"projection": "polar"})
    else:
        result = axis
    levels = np.concatenate(
        (np.array((0, 0.001, 0.01, 0.05)), np.linspace(0.1, 0.99, 9))
    )
    if plot_type == "contour":
        normalized = (function - function.min()) / (function.max() - function.min())
        axis.contourf(
            gamma,
            epsilon_2,
            normalized,
            levels=levels,
            colors=[
                "#cc00ff",
                "#8800ff",
                "#000dff",
                "#009dff",
                "#00d9ff",
                "#00ff95",
                "#aeff00",
                "#fffb00",
                "#ffd000",
                "#ffa200",
                "#ff6a00",
                "#ff0000",
                "#ff0000",
            ],
            **kwargs,
        )
    elif plot_type == "colormesh":
        axis.pcolormesh(gamma, epsilon_2, function, cmap=color_map, **kwargs)
    if add_point_locations_scatter:
        axis.scatter(gamma, epsilon_2, 0.1, color="#ececec")
    if add_minima_scatter:
        min_gamma, min_epsilon_2 = np.unravel_index(np.argmin(function), function.shape)
        axis.scatter(gamma, epsilon_2, 0.1, color="#ececec")
    axis.set_xlim(0, 60 * np.pi / 180)
    axis.grid(visible=grid_lines)
    if axis_labels:
        axis.set_axis_on()
    else:
        axis.set_axis_off()
    return result
