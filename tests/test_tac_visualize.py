from pathlib import Path

from visualize_ibm_2_hamiltonian.tac_visualize import read_tac


def test_read_tac():
    file = Path(__file__).parent.parent / "tac_potentials" / "Cd108.csv"
    results = read_tac(file)
    assert results.shape == (26, 21)