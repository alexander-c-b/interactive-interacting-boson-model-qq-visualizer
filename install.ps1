Import-Module PSToml

$PackageVersion = `
    (Get-Content .\pyproject.toml | ConvertFrom-Toml).project.version

pyinstaller.exe --onefile .\visualize_ibm_2_hamiltonian\__main__.py &&
    Rename-Item .\dist\__main__.exe visualizer_$PackageVersion.exe
pyinstaller.exe --onefile .\visualize_ibm_2_hamiltonian\tac_visualize.py &&
    Rename-Item .\dist\tac_visualize.exe tac_visualizer_$PackageVersion.exe